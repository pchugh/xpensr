#!/usr/local/bin/python3

import argparse
import datetime
import os

import yaml

VERSION = "0.1.0"


class ConfigGenerator:
    @staticmethod
    def __default_config():
        return {
            "line_by_line": [
                {"/Amazon AWS/": "Subscription", "/Amazon/i": "Merchandise"}
            ]
        }

    @staticmethod
    def __write_yaml(location):
        if os.path.isfile(location):
            newlocation = location + ".{}".format(datetime.datetime.now().timestamp())
            os.rename(location, newlocation)
            print("Old configuration file saved as {}".format(newlocation))

        stream = open(location, "w")
        yaml.dump(ConfigGenerator.__default_config(), stream)

    @staticmethod
    def generate():
        filename = ".xpensr.yml"
        location = os.path.join(os.environ.get("HOME"), filename)
        ConfigGenerator.__write_yaml(location)
        return location


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--init",
        action="store_true",
        help="initialize the xpensr.yml configuration file",
    )
    parser.add_argument(
        "-V", "--version", action="version", version="v{}".format(VERSION)
    )
    return parser.parse_args()


def main():
    args = parse_args()
    if args.init:
        config_path = ConfigGenerator.generate()
        print("Generic configuration file created at {}".format(config_path))


if __name__ == "__main__":
    main()
