# xpensr

A command line filter to transform credit card company's CSV files to fit a common system.

## Usage

```sh
$ cat export.csv | xpensr | pbcopy
$ xpensr -h
usage: xpensr [-h] [-i] [-V]

optional arguments:
  -h, --help     show this help message and exit
  -i, --init     initialize the xpensr.yml configuration file
  -V, --version  show program's version number and exit
```
